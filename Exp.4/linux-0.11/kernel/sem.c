#include <unistd.h>
#include <asm/segment.h>
#include <asm/system.h>
#include <linux/kernel.h>
#include <linux/sem.h>

sem_t sem_chain_head = {0, "head", {NULL}, NULL, 0, 0};

/* If founded return PREVIOUS of target,
** or return last one. 
*/
sem_t *get_sem_by_name(const char *name)
{
    int i;
    char name_tmp[20];
    sem_t *sem_tmp = &sem_chain_head;
    /* Read from user data */
    for (i = 0; i != 20; i++)
    {
        name_tmp[i] = get_fs_byte(name + i);
        if (name_tmp[i] == '\0')
            break;
    }
    while (sem_tmp->next != NULL)
    {
        if (strcmp(sem_tmp->next->name, name_tmp) == 0)
            break;
        sem_tmp = sem_tmp->next;
    }
    return sem_tmp;
}

sem_t *sys_sem_open(const char *name, unsigned int value)
{
    sem_t *sem_tmp;
    sem_t *sem_new;
    sem_tmp = get_sem_by_name(name);
    if (strcmp(sem_tmp->next->name, name) == 0)
        return sem_tmp->next;
    /* Not found \|/ */
    sem_new = (sem_t *)malloc(sizeof(sem_t));
    strcpy(sem_new->name, name);
    sem_new->value = value;
    sem_new->next = NULL;
    sem_new->wait_queue_front = 0;
    sem_new->wait_queue_rear = 0;
    sem_tmp->next = sem_new;
    return sem_new;
}

int sys_sem_unlink(const char *name)
{
    sem_t *sem_del;
    sem_t *sem_tmp = get_sem_by_name(name);
    if (strcmp(sem_tmp->next->name, name) == 0)
    {
        sem_del = sem_tmp->next;
        sem_tmp->next = sem_tmp->next->next;
        free(sem_del);
        return 0;
    }
    return -1;
}

int sys_sem_wait(sem_t *sem)
{
    cli();
    sem->value--;
    if (sem->value < 0)
    {
        sem->wait_queue[sem->wait_queue_rear++] = current;
        sem->wait_queue_rear %= NR_TASKS;
        current->state = TASK_UNINTERRUPTIBLE;
        schedule();
    }
    sti();
    return 0;
}

int sys_sem_post(sem_t *sem)
{
    cli();
    struct task_struct *p;
    sem->value++;
    if (sem->value <= 0) /* Some process in wait queue */
    {
        p = sem->wait_queue[sem->wait_queue_front++];
        sem->wait_queue_front %= NR_TASKS;
        p->state = TASK_RUNNING;
    }
    sti();
    return 0;
}
