#include <linux/sched.h>
#define SEM_FAILED (void *)0

typedef struct semaphore_t
{
    int value;
    char name[20];
    struct semaphore_t *next;
    struct task_struct *wait_queue[NR_TASKS];
    unsigned int wait_queue_front;
    unsigned int wait_queue_rear;
} sem_t;
