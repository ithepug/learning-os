#include <stdio.h>
#include <semaphore.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#define CONSUMERS 3
#define BUFSIZE 10
#define N 600

void BufWrite(const int *id, int *pos, const int *data)
{
    lseek(*id, (*pos) * sizeof(int), SEEK_SET);
    write(*id, data, sizeof(int));
    *pos = (*pos + 1) % BUFSIZE;
}

int BufRead(const int *id)
{
    int pos;
    int data;
    // Get pos
    lseek(*id, 10 * sizeof(int), SEEK_SET);
    read(*id, &pos, sizeof(int));
    // Get data
    lseek(*id, pos * sizeof(int), SEEK_SET);
    read(*id, &data, sizeof(int));
    // Update pos
    lseek(*id, 10 * sizeof(int), SEEK_SET);
    pos = (pos + 1) % BUFSIZE;
    write(*id, &pos, sizeof(int));
    return data;
}

int main()
{
    int i, j, k;
    int fid;
    int pos;

    sem_t *mutex;
    sem_t *product;
    sem_t *empty;
    // Open semaphore
    sem_unlink("mutex");
    sem_unlink("product");
    sem_unlink("empty");
    if ((mutex = sem_open("mutex", O_CREAT | O_EXCL, S_IRWXU, 1)) == SEM_FAILED)
    {
        printf("sem mutex open failed!\n");
        return -1;
    }
    if ((product = sem_open("product", O_CREAT | O_EXCL, S_IRWXU, 0)) == SEM_FAILED)
    {
        printf("sem product open failed!\n");
        return -1;
    }
    if ((empty = sem_open("empty", O_CREAT | O_EXCL, S_IRWXU, BUFSIZE)) == SEM_FAILED)
    {
        printf("sem empty open failed!\n");
        return -1;
    }
    // Open buffer file
    if ((fid = open("buffer", O_RDWR | O_CREAT | O_TRUNC, 0666)) == -1)
    {
        printf("file open failed!\n");
        return -1;
    }
    pos = 0;
    // Producer
    if (!fork())
    {
        for (i = 0; i <= N; i++)
        {
            sem_wait(empty);
            sem_wait(mutex);
            //Wirte a integer
            BufWrite(&fid, &pos, &i);
            sem_post(mutex);
            sem_post(product);
        }
        return 0;
    }
    // Initialize read pos
    lseek(fid, 10 * sizeof(int), SEEK_SET);
    write(fid, &pos, sizeof(int));
    // Consumers
    for (j = 0; j != CONSUMERS; j++)
    {
        if (!fork())
        {
            for (k = 0; k < N / CONSUMERS; k++)
            {
                sem_wait(product);
                sem_wait(mutex);
                //Read a integer
                printf("%d\t%d\t%d\n", j, getpid(), BufRead(&fid));
                sem_post(mutex);
                sem_post(empty);
                fflush(stdout);
            }
            return 0;
        }
    }

    for (i = 0; i != 4; i++)
        printf("%d done!\n", wait(NULL));

    sem_unlink("mutex");
    sem_unlink("product");
    sem_unlink("empty");

    close(fid);
    return 0;
}