#include <errno.h>
#define __LIBRARY__
#include <unistd.h>
#include <stdio.h>

_syscall1(int, iam, const char*, name);
_syscall2(int, whoami, char*, name, unsigned int ,size);

int main(int argc,char ** argv)
{
    char s[30];
    iam(argv[1]);
    whoami(s,30);
    printf("%s\n",s);
    return 0;
}
