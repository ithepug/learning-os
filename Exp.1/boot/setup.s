INITSEG=0x9000
entry _start
_start:
!读入光标位置
    mov ah,#0x03
    xor bh,bh
    int 0x10

    mov cx,#14		!显示字符数
    mov bx,#0x0007
    mov bp,#msg2		!指向要显示的字符串
    mov ax,cs
    mov es,ax		!将es段寄存器置为cs
    mov ax,#0x1301
    int 0x10

    mov ax,cs
    mov es,ax

!初始化ss:sp
    mov ax,#INITSEG
    mov ss,ax
    mov sp,#0xFF00

!获取
    mov ax,#INITSEG
    mov ds,ax
    !获取光标位置
    mov ah,#0x03
    xor bh,bh
    int 0x10
    mov [0],dx
    !获取内存大小
    mov ah,#0x88
    int 0x15
    mov [2],ax
    !获取磁盘信息
    mov ax,#0x0000
    mov ds,ax
    lds si,[4*0x41]
    mov ax,#INITSEG
    mov es,ax
    mov di,#0x0004
    mov cx,#0x10
    rep
    movsb

!准备输出
    mov ax,cs
    mov es,ax
    mov ax,#INITSEG
    mov ds,ax

!光标位置
    mov ah,#0x03
    xor bh,bh
    int 0x10
    mov cx,#18
    mov bx,#0x0007
    mov bp,#msg_cursor
    mov ax,#0x1301
    int 0x10
    mov dx,[0]
    call	print_hex

!内存大小
    mov ah,#0x03
    xor bh,bh
    int 0x10
    mov cx,#14
    mov bx,#0x0007
    mov bp,#msg_memory
    mov ax,#0x1301
    int 0x10
    mov dx,[2]
    call	print_hex

!添加KB
    mov ah,#0x03
    xor bh,bh
    int 0x10
    mov cx,#2
    mov bx,#0x0007
    mov bp,#msg_kb
    mov ax,#0x1301
    int 0x10

!Cyles
    mov ah,#0x03
    xor bh,bh
    int 0x10
    mov cx,#7
    mov bx,#0x0007
    mov bp,#msg_cyles
    mov ax,#0x1301
    int 0x10
    mov dx,[4]
    call	print_hex

!Heads
    mov ah,#0x03
    xor bh,bh
    int 0x10
    mov cx,#8
    mov bx,#0x0007
    mov bp,#msg_heads
    mov ax,#0x1301
    int 0x10
    mov dx,[6]
    call 	print_hex

!Secotrs
    mov ah,#0x03
    xor bh,bh
    int 0x10
    mov cx,#10
    mov bx,#0x0007
    mov bp,#msg_sectors
    mov ax,#0x1301
    int 0x10
    mov dx,[12]
    call 	print_hex

inf_loop:
    jmp inf_loop

print_hex:
    mov 	cx,#4

print_digit:
    rol	dx,#4
    mov	ax,#0xe0f
    and	al,dl
    add	al,#0x30
    cmp	al,#0x3a
    jl	outp
    add	al,#0x07

outp:
    int 0x10
    loop	print_digit
    ret

print_nl:
    mov	ax,#0xe0d
    int 	0x10
    mov 	al,#0xa
    int 0x10
    ret

msg2:
    .byte	13,10
    .ascii	"Setup..."
    .byte	13,10,13,10

msg_cursor:
        .byte 13,10
        .ascii "Cursor position:"

msg_memory:
        .byte 13,10
        .ascii "Memory Size:"

msg_cyles:
        .byte 13,10
        .ascii "Cyls:"

msg_heads:
        .byte 13,10
        .ascii "Heads:"

msg_sectors:
        .byte 13,10
        .ascii "Sectors:"

msg_kb:
        .ascii "KB"

.org 510		!使引导扇区标志在最后2个字节

!启动盘具有有效引导扇区的标志
boot_flag:
    .word	0xAA55
