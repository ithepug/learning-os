SETUPLEN=2
SETUPSEG=0x07e0
entry _start
_start:
!读入光标位置
	mov ah,#0x03
	xor bh,bh
	int 0x10

	mov cx,#19		!显示字符数
	mov bx,#0x0007
	mov bp,#msg1		!指向要显示的字符串
	mov ax,#0x7c0
	mov es,ax		!将es段寄存器置为0x7c00
	mov ax,#0x1301
	int 0x10

load_setup:
	mov dx,#0x0000
	mov cx,#0x0002
	mov bx,#0x0200
	mov ax,#0x0200+SETUPLEN	
	int 0x13
	jnc ok_load_setup
	mov dx,#0x0000
	mov ax,#0x0000
	int 0x13
	jmp load_setup

ok_load_setup:
	jmpi	0,SETUPSEG

inf_loop:
	jmp inf_loop

msg1:
	.byte	13,10
	.ascii	"Loading OS..."
	.byte	13,10,13,10

.org 510		!使引导扇区标志在最后2个字节

!启动盘具有有效引导扇区的标志
boot_flag:
	.word	0xAA55
